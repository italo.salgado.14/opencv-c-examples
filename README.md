## openCV

Implementaciones de openCV con C++. Basado en Libro.

> OpenCV 4 Computer Vision Application Programming Cookbook - Fourth Edition - David Millán Escrivá, , Robert Laganiere.

Estos algoritmos se basan en la investigación y exploración de un libro de programación. Aquí se expanden tópicos de implementación de openCV para C++,